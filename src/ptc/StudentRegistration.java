/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ptc;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.*;
import javafx.print.Printer;
import java.io.*;
import java.io.IOException;
/**
 *
 * @author sumay
 */
public class  StudentRegistration extends Frame {
    JLabel l1, l2, l3, l4,l5, l6, l7, l8,l9, l10,l11;	
    JTextField tf1, tf2, tf3,tf4;
    JTextArea area2, area1;
    JRadioButton rb1, rb2;
    JButton b,b2,Print,Receipt;
    JCheckBox cb1,cb2,cb3,cb4,cb8,cb9,cb10,cb11,cb12,cb13,cb14,cb15,cb16,cb17;
    JFileChooser f1;
    
    
StudentRegistration()
	{

		l1 = new JLabel("Student Registration");
		l1.setBounds(550, 50, 250, 20);

		l2 = new JLabel(
			"Student Name:");
		l2.setBounds(50, 100, 250, 20);

		tf1 = new JTextField();
		tf1.setBounds(250, 100, 250, 20);
                
                l3 = new JLabel(
			"Date of Birth:");
		l3.setBounds(50, 150, 250, 20);

		tf2 = new JTextField();
		tf2.setBounds(250, 150, 250, 20);
                
                l4 = new JLabel("Address:");
		l4.setBounds(50, 200, 250, 20);
                
                tf3 = new JTextField();
		tf3.setBounds(250, 200, 250, 20);
                
                l5 = new JLabel("Phone:");
		l5.setBounds(50, 250, 250, 20);

		tf4 = new JTextField();
		tf4.setBounds(250, 250, 250, 20);
                
                
                
                l6 = new JLabel("Gender:");
		l6.setBounds(50, 350, 250, 20);

		JRadioButton rb1
			= new JRadioButton("Male");
		JRadioButton rb2
			= new JRadioButton("Female");

		rb1.setBounds(250, 350, 100, 30);
		rb2.setBounds(350, 350, 100, 30);

		ButtonGroup bg = new ButtonGroup();
		bg.add(rb1);
		bg.add(rb2);

                l7 = new JLabel("Subjects offered:");
		l7.setBounds(50, 400, 250, 20);
                cb1 = new JCheckBox("English");
                cb1.setBounds(50,420,250,20);
                cb2 = new JCheckBox("Math");
                cb2.setBounds(50,440,250,20);
                cb3 = new JCheckBox("Verbal Reasoning");
                cb3.setBounds(50,460,250,20);
                cb4 = new JCheckBox("Non verbal Reasoning");
                cb4.setBounds(50,480,250,20);
                
                l8 = new JLabel("Session:");
		l8.setBounds(600, 100, 250, 20);
                cb12 = new JCheckBox("Morning");
                cb12.setBounds(600,120,250,20);
                cb13 = new JCheckBox("Afternoon");
                cb13.setBounds(600,140,250,20);
                cb14 = new JCheckBox("Evening");
                cb14.setBounds(600,160,250,20);
                
                l9 = new JLabel("Timings:");
		l9.setBounds(600, 200, 250, 20);
                
                cb15 = new JCheckBox("9:30 AM to 10:30 AM");
                cb15.setBounds(600,220,250,20);
                cb16 = new JCheckBox("2:30 PM to 3:30 PM");
                cb16.setBounds(600,240,250,20);
                cb17 = new JCheckBox("6:30 PM to 7:30 PM ");
                cb17.setBounds(600,260,250,20);
                
                l10 = new JLabel("Days:");
		l10.setBounds(600,290, 250, 20);
                
                cb8 = new JCheckBox("Saturday");
                cb8.setBounds(600,310,250,20);
                cb9 = new JCheckBox("Sunday");
                cb9.setBounds(600,330,250,20);
                
                l11 = new JLabel("Books:");
		l11.setBounds(600, 350, 250, 20);
                
                cb10 = new JCheckBox("Mental Math 1");
                cb10.setBounds(600,370,250,20);
                cb11 = new JCheckBox("English Compprehension 3");
                cb11.setBounds(600,390,250,20);
                
                
                JButton b = new JButton("Show");
		b.setBounds(1000, 300, 80, 30);
                
                JButton Receipt
			= new JButton("Generate Receipt");
		Receipt.setBounds(600, 490, 150, 30);
		JButton b2 = new JButton("Reset");
		b2.setBounds(750, 490, 150, 30);
		JButton Print = new JButton("Print");
		Print.setBounds(900, 490, 150, 30);
                
                area2 = new JTextArea();
		area2.setBounds(600, 540, 450, 240);
                
                add(l1);
		add(l2);
		add(l3);
		add(l4);
		add(l5);
		add(l6);
		add(l7);
		add(l8);
		add(l9);
		add(l10);
		add(l11);
                add(tf1);
		add(tf2);
		add(tf3);
                add(tf4);
               
		add(area2);
                add(rb1);
		add(rb2);
                add(cb1);
                add(cb2);
                add(cb3);
                add(cb4);
                add(cb8);
                add(cb9);
                add(cb10);
                add(cb11);
                add(cb12);
                add(cb13);
                add(cb14);
                add(cb15);
                add(cb16);
                add(cb17);
                add(Receipt);
                add(b);
		add(b2);
		add(Print);
                
               b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				
								if (cb8.isSelected()) {
					area2.setText(area2.getText()
								+ "You have chosen Saturday \n");
				}
				if (cb9.isSelected()) {
					area2.setText(area2.getText()
								+ "You have chosen Sunday \n");
				}
			}
		});

		b2.addActionListener(
			new ActionListener() {
				public void actionPerformed(
					ActionEvent e)
				{
					area2.setText("");
					
					tf1.setText("");
					tf2.setText("");
					tf3.setText("");
                                        tf4.setText("");
					
					
				}
			});
			                                         
		Print.addActionListener(
			new ActionListener() {
				public void actionPerformed(
					ActionEvent e)
				{
					try {
						area2.print();
					}
					catch (java.awt.print
							.PrinterException a) {
						System.err.format(
							"NoPrinter Found",
							a.getMessage());
					}
				}
			});

		Receipt.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{

				area2.setText(
					"--------------------------------"
					+ "-----------Student details----"
					+ "--------------------------"
					+ "--------------------------"
					+ "-------------------\n");

				area2.setText(area2.getText()
							+ "Student Name: "
							+ tf1.getText()
							+ "\n");
				area2.setText(area2.getText()
							+ "Date Of Birth: "
							+ tf2.getText()
							+ "\n");
				
				area2.setText(area2.getText()
							+ "Address: "
							+ tf3.getText()
							+ "\n");
                                area2.setText(area2.getText()
							+ "Phone: "
							+ tf4.getText()
							+ "\n");
				

				if (rb1.isSelected()) {
					area2.setText(area2.getText()
								+ "You are a Male");
				}
				if (rb2.isSelected()) {
					area2.setText(area2.getText()
								+ "You are a Female");
				}
				if (cb1.isSelected()) {
					area2.setText(area2.getText()
								+ "You have chosen English \n");
				}
				if (cb2.isSelected()) {
					area2.setText(area2.getText()
								+ "You have chosen Math\n");
				}
                                if (cb3.isSelected()) {
					area2.setText(area2.getText()
								+ "You have chosen Verbal Reasoning\n");
				}
                                if (cb2.isSelected()) {
					area2.setText(area2.getText()
								+ "You have chosen Non Verbal Reasoning\n");
				}
				
				if (e.getSource() == Receipt) {
					try {
						FileWriter fw
							= new FileWriter(
								"java.txt", true);
						fw.write(area2.getText());
						fw.close();
					}
					catch (Exception ae) {
						System.out.println(ae);
					}
				}

				JOptionPane.showMessageDialog(
					area2, "DATA SAVED SUCCESSFULLY");
			};
		});
		addWindowListener(
			new WindowAdapter() {
				public void windowClosing(
					WindowEvent we)
				{
					System.exit(0);
				}
			});
		setSize(800, 800);
		setLayout(null);
		setVisible(true);
		setBackground(Color.cyan);
	}
        
	public static void main(String[] args) {

		new StudentRegistration();
        }	
}
                        
    
                
                          
                      