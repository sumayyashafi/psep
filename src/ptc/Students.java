    package ptc;

/**
 *
 * @author sumay
 */
class Students {
String Name;
int ID;
String Gender;
String Address;
String Phone;
String DoB;
public static String s1Subject1;
public static String s1Sub1Status;
public static int studentcount;

public Students (int ID, String Name) {
    this.ID = ID;
    this.Name = Name;
}
public Students (int ID, String Name, String Gender, String Address, String Phone,String DoB){
    
    this.ID = ID;
    this.Name = Name;
    this.Gender = Gender;
    this.Address = Address;
    this.Phone = Phone;
    this.DoB = DoB;
          
}

    Students() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
public int getID(){
    return ID;
}
public void setID (int ID) {
    this.ID = ID;
}
public String getName() {
    return Name;
}
public void setName (String Name) {
    this.Name = Name;
}
public String getGender(){
    return Gender;
}
public void setGender (String Gender) {
    this.Gender = Gender;
}
public String getAddress() {
    return Address;
}
public void setAddress (String Address) {
    this.Address = Address;
}
public String getPhone() {
    return Phone;
}
public void setPhone (String Phone) {
    this.Phone = Phone;
}
public String getDoB(){
    return DoB;
}
public void setDoB (String DoB) {
    this.DoB = DoB;
}
public void printInfo(){
    System.out.println("Student ID: "+getID() + " " + "Student Name: "+getName() + " " + "StudentGender: "+getGender()  + " " + "Student Address: "+getAddress() + " " + "Student Phone: "+getPhone() + " " + " " + "StudentDoB: " +getDoB());
}

 public static void main(String[] args) {
        
        Students s1 = new Students (101,"Hailey Smith","Female","Holburn Street", "5186290127", "22/02/2011");
        Students s2 = new Students (102,"Brian Hudson","Male","Oxford Street","7292147500" , "01/01/2011");
        Students s3 = new Students (103,"Tom Klein","Male","Abbey Road", "7703598588", "23/08/2012");
        Students s4 = new Students (104,"Dave Tucker","Male","Baker Street", "8572733103", "30/09/2011");
        Students s5 = new Students (105,"Carla Blair","Female","Downing Street", "9197678175", "14/07/2012");
        Students s6 = new Students (106,"Thomas Green","Male","Piccadilly", "2105499319", "28/04/2012");
        Students s7 = new Students (107,"Pena Mylie","Female","Bond Street", "6594882210", "19/06/2012");
        Students s8 = new Students (108,"Hazel Butler","Female","Kings Road", "2011201200", "12/12/2012");
        Students s9 = new Students (109,"Jamie Conley","Male","Carnaby Street", "8105643000", "30/05/2011");
        Students s10 = new Students (110,"Lincoln Zavala","Male","Shaftesbury Avenue","132583692", "03/03/2011");
        Students s11 = new Students (111,"Drake James","Male","Portobello Road","6896938755", "10/10/2011");
        Students s12 = new Students (112,"Lilly Hommes","Female","Holburn Street","1857899632", "11/11/2011"); 
        Students s13 = new Students (113,"Alyssa Ashley","Female","Brick Lane","1470251235", "30/10/2012"); 
        Students s14 = new Students (114,"Megan Oliver","Female","Jermyn Street","3213699870", "28/12/2011"); 
        Students s15 = new Students (115,"Benjamin Lucas","Male","Savile Row","7897411230", "08/08/2012"); 

        s1.printInfo();
        s2.printInfo();
        s3.printInfo();
        s4.printInfo();
        s5.printInfo();
        s6.printInfo();
        s7.printInfo();
        s8.printInfo();
        s9.printInfo();
        s10.printInfo();
        s11.printInfo();
        s12.printInfo();
        s13.printInfo();
        s14.printInfo();
        s15.printInfo();
   
    
}
}

